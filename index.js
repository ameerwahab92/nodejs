const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const methodOverRide = require('method-override');

const app = express();
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(methodOverRide());
app.use(cors());

const port = 3000


var jwt = require('jsonwebtoken');
var token = jwt.sign({ foo: 'bar' }, 'shhhh');


app.get('/login', (req, res) => {

    let requestData = req;
    console.log(requestData);
    res.json({
        sucess: true,
        message: 'Authentication successfully',
        token: token
    })
    // res.send('Hello world')
})

app.post('/', (req, res) => {

    let requestData = req;
    console.log(requestData);
    res.send("Got a post request")
})

app.put('/user', (req, res) => {
    res.send('Got a put request at user')
})

app.delete('/user', (req, res) => {
    res.send('Got a delete request at user');
})

app.listen(port, () => console.log(`App start listening on port ${port}!`))
